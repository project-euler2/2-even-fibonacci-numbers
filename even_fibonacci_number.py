def even_fibonacci_number(number_limit):
    """
    Takes an upper limit as input
    Will return the sum of the even numbers of the fibonacci sequence below that upper limit
    """
    a = 1
    b = 2
    sum_fibonacci = 0
    while a <= number_limit:
        if a % 2 == 0 : sum_fibonacci += a
        if b % 2 == 0 : sum_fibonacci += b 
        a += b
        b += a
    return sum_fibonacci

print(even_fibonacci_number(4000000))